package com.example.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), NewTeamActivity.class);
                startActivity(intent);
            }
        });
        final SportDbHelper dbHelper = new SportDbHelper(this);
        dbHelper.populate();
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this,
            android.R.layout.simple_list_item_2,
            dbHelper.fetchAllTeams(),
            new String[] { SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.COLUMN_LEAGUE_NAME },
        new int[] { android.R.id.text1, android.R.id.text2});
        final ListView lv = (ListView) findViewById(R.id.lstView);
        lv.setAdapter(cursorAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Intent intent = new Intent(parent.getContext(), TeamActivity.class);
                final Team team =  dbHelper.cursorToTeam((Cursor)parent.getItemAtPosition(position));
                long idTeam = team.getId();
                Team teamIntent = dbHelper.getTeam((int) idTeam);
                intent.putExtra("team", team);

                Toast.makeText(MainActivity.this, "Vous avez sélectionné: " + team.getId(), Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
