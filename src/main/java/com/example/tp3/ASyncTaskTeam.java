package com.example.tp3;

import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.TextView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

@RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
public class ASyncTaskTeam extends AsyncTask <Team, String, Boolean> {
    private String TAG;
    private TextView tv;
    protected Boolean doInBackground(Team... teams){
        JSONResponseHandlerTeam teamHandler = new JSONResponseHandlerTeam(teams[0]);
        URL url = null;
        try {
            url = WebServiceUrl.buildSearchTeam(teams[0].getName());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        URLConnection conn = null;
        try {
            conn = url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setAllowUserInteraction(true);
        try {
            conn.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            teamHandler.readJsonStream(conn.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
}
